/**
 * @file ubot_calculate.js
 *
 */
(function ($, Drupal, drupalSettings, once) {
  function ubot_calculate(settings) {
    var ubot_type = [], ubot_floor_type = [], ubot_cell_type = [],
      ubot_thick_type = [];
    if (!$.isEmptyObject(settings.erp.ubot_type)) {
      $.each(settings.erp.ubot_type, function (key, term) {
        ubot_type[term.tid] = {
          volume: parseFloat(term.field_ubot_concrete_volume),
          price: parseFloat(term.field_ubot_price),
        };
        let temp = term.field_ubot_thicks.split(',');
        temp.forEach(element => ubot_thick_type[element] = term.tid);
      });
    }

    if (!$.isEmptyObject(settings.erp.ubot_cell_type)) {
      $.each(settings.erp.ubot_cell_type, function (key, term) {
        ubot_cell_type[term.tid] = {
          name: term.name,
          hscx: parseFloat(term.field_ubot_hscx),
          hsnx: parseFloat(term.field_ubot_hsnx),
          hscy: parseFloat(term.field_ubot_hscy),
          hsny: parseFloat(term.field_ubot_hsny),
        };
      });
    }

    if (!$.isEmptyObject(settings.erp.ubot_floor_type)) {
      $.each(settings.erp.ubot_floor_type, function (key, term) {
        ubot_floor_type[term.tid] = {
          name: term.name,
          static_load: parseFloat(term.field_ubot_static_load),
          wall: parseInt(term.field_ubot_wall),
          dynamic_load: parseFloat(term.field_ubot_dynamic_load),
          psi2: parseFloat(term.field_ubot_psi2),
          hs2: parseFloat(term.field_ubot_hs2),
        };
      });
    }

    function calculate_table(table, steel_grade) {
      $(table + " table input, " + table + " table select").on("change keyup", function () {
        let lx = parseFloat($(this).closest("tr").find(".field_ubot_cal_lx input").val());
        let ly = parseFloat($(this).closest("tr").find(".field_ubot_cal_ly input").val());
        let floor_type_id = $(this).closest("tr").find(".field_ubot_cal_floor_type select").val();
        let hs2 = 0;
        let hs1 = 33;
        if ($.isNumeric(floor_type_id)) {
          hs2 = ubot_floor_type[floor_type_id].hs2;
          let static_load = parseFloat(ubot_floor_type[floor_type_id].static_load) + parseFloat(ubot_floor_type[floor_type_id].wall);
          $(this).closest("tr").find(".field_ubot_cal_static_load input").val(static_load);
          let dynamic_load = parseFloat(ubot_floor_type[floor_type_id].dynamic_load);
          $(this).closest("tr").find(".field_ubot_cal_dynamic_load input").val(dynamic_load);
        }
        let cell_type_id = $(this).closest("tr").find(".field_ubot_cal_cell_type select").val();
        let beams = $(this).closest("tr").find(".field_ubot_cal_beams select").val();
        let cell_type = '';
        if ($.isNumeric(cell_type_id)) {
          cell_type = ubot_cell_type[cell_type_id].name;
        }
        if (cell_type === "Góc" || (lx < ly && cell_type === "Biên //X") || (lx > ly && cell_type === "Biên //Y")) {
          hs1 = 30;
        }
        let thick = '';
        let check_type = '';
        if (beams === 1) {
          check_type = Math.min(lx, ly) / 6;
          thick = Math.min(lx, ly) / 35;
        } else {
          check_type = Math.max(lx, ly) / 6;
          thick = (Math.max(lx, ly) / hs1) * hs2;
        }
        thick = thick.toFixed(2) * 1000;
        $(this).closest("tr").find(".field_ubot_cal_floor_thick input").val(thick);
        $(this).closest("tr").find(".field_ubot_cal_type select").val(ubot_thick_type[thick]);
        let cap = check_type.toFixed(1) * 1000;
        $(this).closest("tr").find(".field_ubot_cal_cap input").val(cap);

        sum_total(table, steel_grade);
      });
    }

    function sum_total(table, steel_grade) {
      let total_volume_concret = [];
      let weight_steel = [];
      let floor_area = [];
      let qty_box = [];
      let price_box = [];
      $(table + ' tbody  > tr').each(function () {
        let lx = parseFloat($(this).find(".field_ubot_cal_lx input").val());
        let ly = parseFloat($(this).find(".field_ubot_cal_ly input").val());
        let static_load = parseFloat($(this).find(".field_ubot_cal_static_load input").val());
        let dynamic_load = parseFloat($(this).closest("tr").find(".field_ubot_cal_dynamic_load input").val());
        let cell_type_id = $(this).find(".field_ubot_cal_cell_type select").val();
        let hscx = 0, hsnx = 0, hscy = 0, hsny = 0;
        if ($.isNumeric(cell_type_id)) {
          hscx = ubot_cell_type[cell_type_id].hscx;
          hsnx = ubot_cell_type[cell_type_id].hsnx;
          hscy = ubot_cell_type[cell_type_id].hscy;
          hsny = ubot_cell_type[cell_type_id].hsny;
        }

        let beams = parseInt($(this).find(".field_ubot_cal_beams select").val());
        let qty = parseInt($(this).find(".field_ubot_cal_qty input").val());
        let thick = parseInt($(this).find(".field_ubot_cal_floor_thick input").val());
        let cap = parseInt($(this).find(".field_ubot_cal_cap input").val());

        let var_total_steel = 1.1;
        let var_box = 1;
        let var_volume_concrete = 1;
        if (beams === 1) {
          var_total_steel = 1.2;
          var_box = 0.9;
          var_volume_concrete = 1.1;
        }

        let btx = ly * thick / 1000 * 20 * 1.35;
        let ttx = ly * static_load * 1.35;
        let htx = ly * dynamic_load * 1.5;
        let qx = btx + ttx + htx;
        let Mcx_ = qx * Math.pow(lx, 2) / 8 * 0.65 * hscx;
        let Acx_ = Mcx_ / 0.087 / steel_grade / (thick / 1000 - 0.02);
        let Mcx = qx * Math.pow(lx, 2) / 8 * 0.65 * (1 - hscx);
        let Acx = Mcx / 0.087 / steel_grade / (thick / 1000 - 0.02);
        let Mnx_ = qx * Math.pow(lx, 2) / 8 * 0.35 * hsnx;
        let Anx_ = Mnx_ / 0.087 / steel_grade / (thick / 1000 - 0.02);
        let Mnx = qx * Math.pow(lx, 2) / 8 * 0.35 * (1 - hsnx);
        let Anx = Mnx / 0.087 / steel_grade / (thick / 1000 - 0.02);
        let grade_steel_x = (Anx + Acx) * lx * 0.785;
        let upper_layer_steel_x = (Anx_ + Acx_) * cap * 0.002 * 0.785;
        let bty = lx * thick / 1000 * 20 * 1.35;
        let tty = lx * static_load * 1.35;
        let hty = lx * dynamic_load * 1.5;
        let qy = bty + tty + hty;
        let Mcy_ = qy * Math.pow(ly, 2) / 8 * 0.65 * hscy;
        let Acy_ = Mcy_ / 0.087 / steel_grade / (thick / 1000 - 0.02);
        let Mcy = qy * Math.pow(ly, 2) / 8 * 0.65 * (1 - hscy);
        let Acy = Mcy / 0.087 / steel_grade / (thick / 1000 - 0.02);
        let Mny_ = qy * Math.pow(ly, 2) / 8 * 0.35 * hsny;
        let Any_ = Mny_ / 0.087 / steel_grade / (thick / 1000 - 0.02);
        let Mny = qy * Math.pow(ly, 2) / 8 * 0.35 * (1 - hsny);
        let Any = Mny / 0.087 / steel_grade / (thick / 1000 - 0.02);
        let grade_steel_y = (Any + Acy) * ly * 0.785;
        let upper_layer_steel_y = (Any_ + Acy_) * cap * 0.002 * 0.785;
        let resistant_steel = cap / 15;

        let cal_total_steel = (grade_steel_x + upper_layer_steel_x + grade_steel_y + upper_layer_steel_y + resistant_steel) * qty * var_total_steel;
        weight_steel.push(cal_total_steel);
        let qtybox = Math.round((lx * ly - Math.pow(cap / 1000, 2) * 4) / 0.4356) * qty * var_box;
        qty_box.push(qtybox);

        let box_type_id = parseInt($(this).find(".field_ubot_cal_type select").val());
        let cal_total_concrete = 0;
        if ($.isNumeric(box_type_id)) {
          let total_box_weight = ubot_type[box_type_id].volume * qtybox;
          let box_price_unit = ubot_type[box_type_id].price * qtybox;
          price_box.push(box_price_unit);
          cal_total_concrete = (lx * ly * qty * thick / 1000 - total_box_weight) * var_volume_concrete;
        }
        total_volume_concret.push(cal_total_concrete);
        floor_area.push(lx * ly * qty);

      });
      let total_concret = 0;
      $.each(total_volume_concret, function () {
        total_concret += this;
      });

      let total_floor_area = 0;
      $.each(floor_area, function () {
        total_floor_area += this;
      });

      let total_weight_steel = 0;
      $.each(weight_steel, function () {
        total_weight_steel += this;
      });

      let total_qty_box = 0;
      $.each(qty_box, function () {
        total_qty_box += this;
      });

      let total_price_box = 0;
      $.each(price_box, function () {
        total_price_box += this;
      });

      $(".field--name-field-ubot-quote input.tablefield-row-1.tablefield-col-2").val(total_concret.toFixed(0));
      $(".field--name-field-ubot-quote input.tablefield-row-2.tablefield-col-2").val(total_floor_area * 1.2);
      $(".field--name-field-ubot-quote input.tablefield-row-3.tablefield-col-2").val(total_weight_steel.toFixed(0));
      $(".field--name-field-ubot-quote input.tablefield-row-4.tablefield-col-2").val(total_qty_box.toFixed(0));
      $(".field--name-field-ubot-quote input.tablefield-row-4.tablefield-col-4").val(total_price_box.toFixed(0));
      let construction_price_unit = total_price_box / total_qty_box;
      $(".field--name-field-ubot-quote input.tablefield-row-4.tablefield-col-3").val(construction_price_unit.toFixed(0));
    }

    function sum_quote() {
      let volume_concrete = parseFloat($(".field--name-field-ubot-quote input.tablefield-row-1.tablefield-col-2").val());
      let volume_formwork = parseFloat($(".field--name-field-ubot-quote input.tablefield-row-2.tablefield-col-2").val());
      let volume_steel = parseFloat($(".field--name-field-ubot-quote input.tablefield-row-3.tablefield-col-2").val());
      let volume_ubot = parseFloat($(".field--name-field-ubot-quote input.tablefield-row-4.tablefield-col-2").val());
      let price_concrete = parseFloat($(".field--name-field-ubot-quote input.tablefield-row-1.tablefield-col-3").val());
      let price_formwork = parseFloat($(".field--name-field-ubot-quote input.tablefield-row-2.tablefield-col-3").val());
      let price_steel = parseFloat($(".field--name-field-ubot-quote input.tablefield-row-3.tablefield-col-3").val());
      let price_ubot = parseFloat($(".field--name-field-ubot-quote input.tablefield-row-4.tablefield-col-3").val());

      let total_price_concrete = volume_concrete * price_concrete;
      let total_price_formwork = volume_formwork * price_formwork;
      let total_price_steel = volume_steel * price_steel;
      let total_price_ubot = parseFloat($(".field--name-field-ubot-quote input.tablefield-row-4.tablefield-col-4").val());

      let total_price_floor = total_price_concrete + total_price_formwork + total_price_steel + total_price_ubot;
      let total_build_cost = total_price_floor / (volume_formwork / 1.2);
      $(".field--name-field-ubot-quote input.tablefield-row-1.tablefield-col-4").val(total_price_concrete.toFixed(0));
      $(".field--name-field-ubot-quote input.tablefield-row-2.tablefield-col-4").val(total_price_formwork.toFixed(0));
      $(".field--name-field-ubot-quote input.tablefield-row-3.tablefield-col-4").val(total_price_steel.toFixed(0));
      $(".field--name-field-ubot-quote input.tablefield-row-4.tablefield-col-4").val(total_price_ubot.toFixed(0));
      $(".field--name-field-ubot-quote input.tablefield-row-5.tablefield-col-4").val(total_price_floor.toFixed(0));
      $(".field--name-field-ubot-quote input.tablefield-row-6.tablefield-col-4").val(total_build_cost.toFixed(0));
      $(".field--name-field-crm-expected input").val(total_price_floor.toFixed(0));
    }

    $(".field--name-field-ubot input").addClass("text-end");
    $(".field--name-field-ubot input,.field--name-field-ubot select").on("keyup change", function () {
      let concrete_grade = $(".field--name-field-ubot-concrete-grade select").val();
      let steel_grade = $(".field--name-field-ubot-steel-grade select").val();

      $(".field--name-field-ubot-quote input.tablefield-row-1.tablefield-col-3").val(concrete_grade);
      let table_class = ".field--name-field-ubot-calculate";
      calculate_table(table_class, steel_grade);
      sum_quote();
    });

    $(".field--name-field-ubot-quote input").on("keyup change", function () {
      sum_quote();
    });
  }

  Drupal.behaviors.ubot_calculate = {
    attach: function (context, settings) {
      ubot_calculate(settings);
    }
  }
}(jQuery, Drupal, drupalSettings, once));
