<?php

namespace Drupal\lpc_ubot\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Settings form for lpc_ubot.
 */
class UbotSettingsForm extends ConfigFormBase {

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'lpc_ubot.settings';

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);

    $form['steel_cost_price'] = [
      '#type' => 'number',
      '#title' => $this->t('The steel labor costing'),
      '#default_value' => $config->get('steel_cost_price'),
    ];
    $form['formwork_price'] = [
      '#type' => 'number',
      '#title' => $this->t('Form work price'),
      '#default_value' => $config->get('formwork_price'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config(static::SETTINGS)
      ->set('steel_cost_price', $form_state->getValue('steel_cost_price'))
      ->set('formwork_price', $form_state->getValue('formwork_price'))
      ->save();
  }

}
