<?php

namespace Drupal\lpc_ubot\Plugin\Field\FieldWidget;

use Drupal\Core\Field\Annotation\FieldWidget;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\paragraphs\Plugin\Field\FieldWidget\InlineParagraphsWidget;

/**
 * Plugin implementation of the 'paragraphs_tabs_bootstrap_widget' widget.
 *
 * @FieldWidget(
 *   id = "paragraphs_ubot_calculate_widget",
 *   label = @Translation("Paragraphs ubot quote"),
 *   description = @Translation("Paragraphs Ubot quote Lpc."),
 *   field_types = {
 *     "entity_reference_revisions"
 *   }
 * )
 */
class UbotCalculateWidget extends InlineParagraphsWidget {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);
    $form['#attached']['library'][] = 'lpc_ubot/ubot_calculate';
    $form['#attached']['drupalSettings']['erp']['ubot_type'] = $this->_get_ubot_config('ubot_type');
    $form['#attached']['drupalSettings']['erp']['ubot_floor_type'] = $this->_get_ubot_config('ubot_floor_type');
    $form['#attached']['drupalSettings']['erp']['ubot_cell_type'] = $this->_get_ubot_config('ubot_cell_type');

    $config = \Drupal::config('lpc_ubot.settings');
    $element["subform"]["field_ubot_quote"]["widget"][0]["#default_value"][2][3] =  $config->get('formwork_price');
    $element["subform"]["field_ubot_quote"]["widget"][0]["#default_value"][3][3] =  $config->get('steel_cost_price');

    return $element;
  }

  private function _get_ubot_config($vid) {
    if ($cache = \Drupal::cache()
      ->get($vid)) {
      $ubot_config = $cache->data;
    }
    else {
      $entity_type_id = 'taxonomy_term';
      $bundle_fields = \Drupal::service('entity_field.manager')
        ->getFieldDefinitions($entity_type_id, $vid);
      $terms = \Drupal::entityTypeManager()->getStorage($entity_type_id)->loadByProperties([
        'vid' => $vid
      ]);
      foreach ($terms as $term){
        $config = [];
        foreach ($bundle_fields as $field_name=>$field_config){
          $config[$field_name] = $term->get($field_name)->value;
        }
        $ubot_config[$term->id()] = $config;
      }
      \Drupal::cache()
        ->set($vid, $ubot_config);
    }
    return $ubot_config;
  }

}
