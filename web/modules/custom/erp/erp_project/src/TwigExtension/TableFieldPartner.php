<?php
namespace Drupal\erp_project\TwigExtension;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

class TableFieldPartner extends AbstractExtension {

  /**
   * Gets a unique identifier for this Twig extension.
   *
   * @return string
   *   A unique identifier for this Twig extension.
   */
  public function getName() {
    return 'erp_project.tablefieldpartner';
  }

  public function getFilters() {
    return [
      'tablefieldpartner' => new TwigFilter('tablefieldpartner', ['Drupal\erp_project\TwigExtension\TableFieldPartner', 'filterTableFieldPartner']),
    ];
  }

  public static function filterTableFieldPartner($string) {
    $str = trim(strtolower($string));
    $str = preg_replace('/[^a-z0-9\s+]/', '', $str);
    $str = preg_replace('/\s+/', ' ', $str);
    $str = explode(' ', $str);
    $str = array_map('ucwords', $str);
    return implode(' ', $str);
  }

  public function getFunctions() {
    return [
      new TwigFunction('send_to_partner', [$this, 'sendToPartner']),
      new TwigFunction('receiver_partner', [$this, 'receiverPartner']),
    ];
  }
  public function sendToPartner($entity_id, $fieldReceiver = 'field_receiver', $view_mode = 'preview') {
    $paragraphId = (string) $entity_id;
    $entityType = \Drupal::entityTypeManager();
    $paragraphParent = $entityType->getStorage('paragraph')->load($paragraphId);
    $builds = [];
    $builder = $entityType->getViewBuilder('paragraph');
    if(!empty( $paragraphParent->field_partners)){
      foreach ( $paragraphParent->field_partners as $partner){
        $paragraphId = $partner->target_id;
        $paragraph = $entityType->getStorage('paragraph')->load($paragraphId);
        if(!empty($paragraph->{$fieldReceiver}->value)){
          $builds[] = $builder->view($paragraph, $view_mode);
        }
      }
    }
    return $builds;
  }
  public function receiverPartner($entity_id, $fieldReceiver = 'field_receiver', $fieldDate = 'field_date_created', $view_mode = 'preview') {
    $paragraphId = (string) $entity_id;
    $entityType = \Drupal::entityTypeManager();
    $paragraphParent = $entityType->getStorage('paragraph')->load($paragraphId);
    $header = ['Nơi nhận', 'Đã nhận yêu cầu ngày', 'Người nhận', 'Chữ ký'];
    $rows = [];
    $builder = $entityType->getViewBuilder('paragraph');
    if(!empty( $paragraphParent->field_partners)){
      foreach ( $paragraphParent->field_partners as $partner){
        $paragraphId = $partner->target_id;
        $paragraph = $entityType->getStorage('paragraph')->load($paragraphId);
        if(!empty($paragraph->{$fieldReceiver}->value)){
          $rows[] = [
            \Drupal::service('renderer')->render($builder->view($paragraph, $view_mode)),
            Date('d/m/Y',strtotime($paragraphParent->{$fieldDate}->value)),
            $paragraph->{$fieldReceiver}->value,
            '',
          ];
        }
      }
    }
    $builds = [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#attributes' => [
        'class' => ['table', 'table-bordered']
      ]
    ];
    return $builds;
  }
}
