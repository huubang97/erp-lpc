<?php

namespace Drupal\erp_partner\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\Component\Serialization\Json;


/**
 * Class KanbanController.
 */
class PartnerController extends ControllerBase {

  /**
   * @param string $taxid
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function taxid($taxid = '') {
    $url = \Drupal::request()->query->get('url');

    try {
      $response = \Drupal::httpClient()->get($url.'/'.$taxid , ['headers' => ['charset' => 'utf-8']]);
      $data = $response->getBody();
      if (empty($data)) {
        return FALSE;
      }
    }
    catch (TransferException $e) {
      return FALSE;
    }
    return new JsonResponse(Json::decode($data));
  }

}
