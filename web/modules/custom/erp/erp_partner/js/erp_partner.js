(function ($, Drupal, drupalSettings) {
  Drupal.behaviors.erp_partner = {
    attach: function (context, drupalSettings) {
      $(document, context).once('erp_partner ').each(function () {
        //get api numero siret
        $("input[name*='field_partner_taxcode[0][value]']").on('change', function () {
          var siret = $(this).val().replace(" ", "");
          var $this = $(this);
          if (siret.length >= 9 && siret.length<=13) {
            var dataAPI = '/api/taxid/'+siret+'?url=https://thongtindoanhnghiep.co/api/company' ;
            $.getJSON(dataAPI).done(function (datas) {
              if (datas.MaSoThue !== undefined) {
                if(datas.Title !== undefined){
                  $("input[name*='title[0][value]']").val(datas.Title);
                }
                if(datas.TitleEn !== undefined){
                  $("input[name*='field_partner_name[0][value]']").val(datas.TitleEn);
                }
                if(datas.DiaChiCongTy !== undefined){
                  $("input[name*='field_partner_address[0][address][address_line1]']").val(datas.DiaChiCongTy);
                }

                if(datas.ChuSoHuu !== undefined){
                  $("input[name*='field_partner_representation[0][value]']").val(datas.ChuSoHuu);
                }
                if(datas.VonDieuLe !== undefined){
                  $("input[name*='field_partner_capital[0][value]']").val(datas.VonDieuLe);
                }
                if(datas.NganhNgheTitle !== undefined){
                  $("input[name*='field_partner_type[0][value]']").val(datas.NganhNgheTitle);
                }
              }
            });
          }
        });
      });
    }
  }
}(jQuery, Drupal, drupalSettings));
